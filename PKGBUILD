# Maintainer: Felix Golatofski <contact@xdfr.de>
# Maintainer: Nogweii <me@nogweii.net>
# Contributor: Marcin (CTRL) Wieczorek <marcin@marcin.co>
# Contributor: Xiao-Long Chen <chenxiaolong@cxl.epac.to>

pkgname=certmonger
pkgver=0.79.18
pkgrel=1
pkgdesc="Certificate status monitor and PKI enrollment client"
arch=('x86_64')
url="https://pagure.io/certmonger"
license=('GPL')
depends=(
    'curl'
    'dbus' 
    'e2fsprogs'
    'glibc'
    'gmp'
    'jansson'
    'krb5'
    'libidn2'
    'libldap'
    'libxml2'
    'libxmlrpc'
    'nspr'
    'nss'
    'openssl'
    'popt'
    'systemd'
    'talloc'
    'tevent'
    'util-linux-libs'
)
backup=(
    'etc/certmonger/certmonger.conf'
)
install="${pkgname}.install"
source=("https://pagure.io/certmonger/archive/${pkgver}/certmonger-${pkgver}.tar.gz")
sha512sums=(
    'fff98b0e95f9e7394a8a4b7831f6249f2d0f518a178b9b65c4ff7a3c7fd98f9e3e634019a6a58e76454edaa8773b38bf2058c53cf51c8bf996b16388d5a5288a'
)

prepare() {
    cd "$pkgname-$pkgver"

    # Arch Linux uses /etc/conf.d/
    sed -i 's/sysconfig/conf.d/g' ./systemd/certmonger.service.in

    autoreconf -vfi
}

build() {
    cd "$pkgname-$pkgver"

    ./configure --prefix=/usr \
                --sysconfdir=/etc \
                --sbindir=/usr/bin \
                --libexecdir=/usr/lib/${pkgname} \
                --localstatedir=/var \
                --enable-systemd \
                --enable-tmpfiles \
                --enable-pie \
                --enable-now \
                --with-xmlrpc \
                --with-homedir=/run/certmonger \
                --with-tmpdir=/run/certmonger \
                --with-uuid \
                --with-gmp

    make
}

package() {
    cd "$pkgname-$pkgver"

    make DESTDIR="${pkgdir}/" install

    install -Dpm644 'LICENSE' -t "${pkgdir}/usr/share/licenses/${pkgname}/"
}
